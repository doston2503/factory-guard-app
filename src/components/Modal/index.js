import React from 'react';
import styles from "./Modal.module.scss";
const Modal = ({isOpen, toggle, children, lg, style, xl}) => {
    return (
        <>
            <div className={styles.modal + " " + (isOpen ? styles.active : "") + " " + (lg ? styles.modalLg : "") + " " + (xl ? styles.modalXXl: "")} >
                <div className={styles.modalContent} style={style}>
                    {children}
                </div>
                <img src="/assets/icon/x.svg" alt="x" className={styles.x} onClick={toggle}/>
            </div>
            <div className={styles.modalBackdrop + " " + (isOpen ? styles.active : "")} onClick={toggle}/>
        </>
    );
};

export default Modal;
