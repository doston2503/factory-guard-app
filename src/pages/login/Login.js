import React from 'react';
import axios from "axios";
import {PATH_NAME} from "../../utils/constants";
import {toast} from "react-toastify";
function Login(props) {

    function loginUser(e) {
        e.preventDefault();
        const formData = new FormData();
        formData.append('username', e.target.username?.value);
        formData.append('password', e.target.password?.value);

        const headers = {
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'multipart/form-data',
            },
        };

        axios.post(`${PATH_NAME}token`, formData, headers)
            .then((response) => {
                localStorage.setItem("access_token",response.data?.access_token);
                props.history.push('/');
                console.log(response);
            })
            .catch((error) => {
                toast.error('Login yoki parol xato');
                console.error('Error:', error);
            });
    }


    return (
        <div className="login-page">
            <div className="container">
                <div className="login-page-header">
                    <div className="title">Добро пожаловать в</div>
                    <div className="main-brand">
                        <img src="/assets/images/logo.svg" alt=""/>
                    </div>
                </div>
                <div className="login-page-body">
                    <form onSubmit={loginUser}>
                        <h4>Авторизоваться</h4>

                        <label htmlFor="username">Login</label>
                        <input type="text" id="username"
                               name="username" placeholder="Ваш логин"
                               className="form-control mb-4"/>

                        <label htmlFor="password">Пароль</label>
                        <input type="password" id="password"
                               name="password" placeholder="Ваш пароль"
                               className="form-control"/>

                        <button type="submit">
                            Вход
                        </button>
                    </form>
                </div>
            </div>
        </div>
    );
}

export default Login;