import React from 'react';
import MainLayout from "../../layout/MainLayout";

function FactoryArea(props) {
    return (
        <MainLayout>
            <div className="mainLayoutContentPadding">

                <div className="factory-area-page">
                    <div className="container-fluid p-0">
                        <div className="factory-are-page-header">
                            {/*<img src="/assets/factory.png" alt=""/>*/}
                            <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d4342.620706580206!2d65.32869713708887!3d40.112032491914526!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3f51c700b94595d5%3A0x1bebc851c61c51a7!2z0J3QsNCy0L7QuNCw0LfQvtGC!5e1!3m2!1sru!2s!4v1701439772941!5m2!1sru!2s" style={{border: 0}} allowFullScreen="" loading="lazy" referrerPolicy="no-referrer-when-downgrade"/>                                                        </div>
                        <div className="factory-area-page-body mt-4">
                            <div className="title">
                                Типы уведомлений
                            </div>
                            <div className="d-flex">
                                <div className="notification">
                                    <div className="circle">
                                        <img src="/assets/images/introdur.svg" alt=""/>
                                    </div>
                                    <div className="notification-text">
                                        Обнаружение злоумышленников
                                    </div>
                                </div>
                                <div className="notification">
                                    <div className="circle circle-fall">
                                        <img src="/assets/images/fall.svg" alt=""/>
                                    </div>
                                    <div className="notification-text">
                                        Въезд автомобиля
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </MainLayout>
    );
}

export default FactoryArea;
