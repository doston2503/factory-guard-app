import React, {useEffect, useState} from 'react';
import MainLayout from "../../layout/MainLayout";
import Select from 'react-select';
import axios from "axios";
import {CONFIG, PATH_NAME} from "../../utils/constants";
import ReactPlayer from "react-player";
import {useHistory} from "react-router-dom";

function Cameras(props) {
    let history = useHistory()
    // const [cameras, setCameras] = useState([]);
    // const [selectedCamera, setSelectedCamera] = useState(null);
    const [staticVideo, setStaticVideo] = useState([
        {id: 1, stream_url: "https://hls.cradle-vision.com/stream/factorycamera7/index.m3u8"},
        {id: 2, stream_url: "https://hls.cradle-vision.com/stream/factorycamera9/index.m3u8"},
        {id: 3, stream_url: "https://hls.cradle-vision.com/stream/factorycamera10/index.m3u8"},
        {id: 4, stream_url: "https://hls.cradle-vision.com/stream/factorycamera11/index.m3u8"},
        {id: 5, stream_url: "https://hls.cradle-vision.com/stream/smartcamera9/index.m3u8"},
        {id: 6, stream_url: "https://hls.cradle-vision.com/stream/smartcamera10/index.m3u8"},
        {id: 7, stream_url: "https://hls.cradle-vision.com/stream/factorycamera12/index.m3u8"},
    ]);

    useEffect(() => {
        // getCameraList("");
    }, []);

    // const getCameraList = (search) => {
    //     axios.get(PATH_NAME + "company/camera/smartcamera/all?page=1&per_page=100&camera_type=entry&search_str=" + search, CONFIG)
    //         .then(response => {
    //             setCameras(response.data?.items);
    //             console.log(response.data?.items);
    //         })
    // };

    // const selectCamera = (e) => {
    //
    //     setSelectedCamera(staticVideo.filter(item => item.id == e.value)[0])
    // };

    return (
        <MainLayout>
            <div className="auto-counter-page-feruz">

                <div className="street-page-body-feruz">

                    <div className="row">

                                {
                                    staticVideo?.map((item, index) => (

                                        <div className="col-md-6">
                                            <div className="cameras">
                                                <h4>Camera {index + 1}</h4>
                                           <ReactPlayer onClick={() => history.push("/cameras/" + item.id)} className="video-wrap" url={item?.stream_url} playing={true}
                                                     muted/>

                                            </div>

                                        </div>
                                    ))
                                }
                    </div>
                </div>
            </div>
        </MainLayout>
    );
}

export default Cameras;
