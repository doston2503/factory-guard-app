import React, {useEffect, useState} from 'react';
import MainLayout from "../../layout/MainLayout";
import Select from 'react-select';
import axios from "axios";
import {CONFIG, PATH_NAME} from "../../utils/constants";
import ReactPlayer from "react-player";
import {Link, useHistory, useParams} from "react-router-dom";

function CameraDetail(props) {


    const [staticVideo, setStaticVideo] = useState([
        {id: 1, stream_url: "https://hls.cradle-vision.com/stream/factorycamera7/index.m3u8"},
        {id: 2, stream_url: "https://hls.cradle-vision.com/stream/factorycamera9/index.m3u8"},
        {id: 3, stream_url: "https://hls.cradle-vision.com/stream/factorycamera10/index.m3u8"},
        {id: 4, stream_url: "https://hls.cradle-vision.com/stream/factorycamera11/index.m3u8"},
        {id: 5, stream_url: "https://hls.cradle-vision.com/stream/smartcamera9/index.m3u8"},
        {id: 6, stream_url: "https://hls.cradle-vision.com/stream/smartcamera10/index.m3u8"},
        {id: 7, stream_url: "https://hls.cradle-vision.com/stream/factorycamera12/index.m3u8"},
    ]);
    let params = useParams()

    useEffect(() => {
    }, []);



    return (
        <MainLayout>
            <div className="auto-counter-page-feruz">
                <div className="add-user-page-header">
                    <Link to={"/"}>
                        <img src="/assets/images/home.svg" alt=""/>
                    </Link>
                    <img className="arrow-right-img" src="/assets/images/arrow-right.svg" alt=""/>
                    <Link to={"/cameras"} className="users-link">
                        Камеры
                    </Link>
                    <img className="arrow-right-img" src="/assets/images/arrow-right.svg" alt=""/>
                    <span>Camera {params.id}</span>
                </div>
                <div className="street-page-body-feruz">

                    <div className="row">

                        {/*{*/}
                        {/*    // staticVideo.filter(item => item.id == params.id)[0]?.map((item, index) => (*/}

                                <div className="col-md-12">
                                    <div className="cameras">
                                        <h4>Camera 1</h4>
                                        <ReactPlayer className="video-wrap" url={staticVideo.filter(item => item.id == params.id)[0]?.stream_url} playing={true}
                                                     muted/>

                                    </div>

                                </div>
                            {/*))*/}
                        {/*}*/}
                    </div>
                </div>
                <button type={"button"}
                        className="f-btn mb-3"
                        onClick={() => props.history.push('/cameras')}
                >
                    <img src="/assets/images/arrow-left.svg" alt=""/>
                    Назад
                </button>
            </div>
        </MainLayout>
    );
}

export default CameraDetail;
