const SVGComponent = () => {
    return (
        <Svg>
            {svgData.paths.map((path, index) => (
                <path key={index} d={path.toShapes(true)[0].getPoints()} />
            ))}
        </Svg>
    );
};