import React, {useEffect, useState} from 'react';
import MainLayout from "../../layout/MainLayout";
import Modal from "../../components/Modal";
import axios from "axios";
import {CONFIG, getCurrentDateTime, PATH_NAME} from "../../utils/constants";
import Select from 'react-select';
import {toast} from "react-toastify";
import ReactPlayer from "react-player";

function Register(props) {
    const [isOpen, setIsOpen] = useState(false);
    const [cameras, setCameras] = useState([]);
    const [selectedCamera, setSelectedCamera] = useState(null);
    const [socket, setSocket] = useState(null);
    const [socketData, setSocketData] = useState(null);
    const [currentDateTime, setCurrentDateTime] = useState('');

    useEffect(() => {
        getCameraList("");

        return () => {
           if (socket){
               socket.close();
           }
        };
    }, []);

    const connectToSocket = (cameraId) => {
        if (socket){
            socket.close();
        }
        const newSocket = new WebSocket('ws://178.62.213.208:9001/factorysmart/' + cameraId);

        newSocket.addEventListener('open', (event) => {
            console.log('WebSocket connection opened:', event);
        });

        newSocket.addEventListener('message', (event) => {
            console.log("data: ", event.data)
            const message = JSON.parse(event.data.replace(/'/g, '"'));
            console.log(message);
            console.log("if " , message.main_data && message.from_database)
            if (message.main_data && message.from_database){
                setSocketData(message);
                setCurrentDateTime(getCurrentDateTime());
            }

            console.log('Message from server:', message);
        });

        newSocket.addEventListener('close', (event) => {
            console.log('WebSocket connection closed:', event);
        });

        newSocket.addEventListener('error', (event) => {
            console.error('WebSocket error:', event);
        });

        setSocket(newSocket);
    }

    const getCameraList = (search) => {
        axios.get(PATH_NAME + "company/camera/smartcamera/all?page=1&per_page=100&camera_type=entry&search_str=" + search, CONFIG)
            .then(response => {
                setCameras(response.data?.items);
            })
    }

    const selectCamera = (e) => {
        connectToSocket(e.value);
        setSelectedCamera(cameras.filter(item => item.id == e.value)[0])
    }

    return (
       <MainLayout>
           <div className="register-page">
               <div className="register-page-header d-flex align-items-center justify-content-between">
                   <div>
                       <h3 className="mb-0">Регистрироваться</h3>
                       <p className="mb-0">Проверка всех посетителей</p>
                   </div>
                   <div className="d-flex">
                       <Select
                           className="search-select"
                           classNamePrefix="select"
                           isSearchable={true}
                           placeholder="Камера"
                           onInputChange={e => getCameraList(e)}
                           name="camera"
                           onChange={e => selectCamera(e)}
                           options={cameras?.map(item => {return {label: item.name, value: item.id}})}
                       />
                       <button type="button" className="btn btn-primary" onClick={() => {
                           if (selectedCamera){
                               setIsOpen(true)
                           } else {
                               toast.error("Пожалуйста, выберите камеру");
                           }
                       }}>
                           <img src="/assets/icon/camera.svg" alt="camera"/>
                           Смотреть видео
                       </button>
                   </div>
               </div>
               <div className="register-page-body">
                   {socketData ?
                       <div className="person-info d-flex justify-content-between align-items-center">
                           <div className="person-info-base">
                               <h4>Из базы</h4>
                               {socketData.from_database.image ?
                                   <div className={`person-info-image`}>
                                       <img src={socketData.from_database.image} alt="person"/>
                                   </div> :
                                   <div className={`person-info-image person-info-image-none`}>
                                       {/*<img src="/assets/images/person2.png" alt="person"/>*/}
                                       <img src="/assets/images/noone.png" alt="person"/>
                                   </div>
                               }
                           </div>
                           <div className="person-info-information">
                               <h4>Информация</h4>
                               {socketData.from_database.image ?
                                   <div className={`person-info-box ${socketData.from_database.is_admin ? "person-info-box-admin": "person-info-box-worker"}`}>
                                       <h3 className="text-center">{socketData.from_database.is_admin ? "Администратор": "Рабочий"}</h3>
                                       <div className="person-info-box-content">
                                           <div className="person-info-box-content-item d-flex align-items-center justify-content-between">
                                               <p className="mb-0">Ф.И.О.</p>
                                               <h6 className="mb-0">{socketData.main_data.first_name} {socketData.main_data.last_name}</h6>
                                           </div>
                                           <div className="person-info-box-content-item d-flex align-items-center justify-content-between">
                                               <p className="mb-0">Отделение</p>
                                               <h6 className="mb-0">{socketData.from_database.department}</h6>
                                           </div>
                                           <div className="person-info-box-content-item d-flex align-items-center justify-content-between">
                                               <p className="mb-0">Позиция</p>
                                               <h6 className="mb-0">{socketData.from_database.position}</h6>
                                           </div>
                                           <div className="person-info-box-content-item d-flex align-items-center justify-content-between">
                                               <p className="mb-0">Время входа</p>
                                               <h6 className="mb-0">{currentDateTime}</h6>
                                           </div>
                                       </div>
                                   </div> :
                                   <div className="person-info-box person-info-box-none">
                                       <h3 className="text-center mb-0">Не опознано</h3>
                                       <h2 className="mb-0">Нет данных</h2>
                                       <div className="person-info-box-content">
                                           <div className="person-info-box-content-item d-flex align-items-center justify-content-between">
                                               <p className="mb-0">Время входа</p>
                                               <h6 className="mb-0">{currentDateTime}</h6>
                                           </div>
                                       </div>
                                   </div>
                               }
                               <div className="person-info-information-badges d-flex justify-content-between flex-wrap">
                                   <div className="person-info-information-bade-item d-flex align-items-center">
                                       <div className="circle circle-admin" /> Администратор
                                   </div>
                                   <div className="person-info-information-bade-item d-flex align-items-center">
                                       <div className="circle circle-not-recognized" /> Не опознано
                                   </div>
                                   <div className="person-info-information-bade-item d-flex align-items-center">
                                       <div className="circle circle-worker" /> Рабочий
                                   </div>
                               </div>
                           </div>
                           <div className="person-info-base">
                               <h4>Скриншот</h4>
                               <div className="person-info-image">
                                   <img src={socketData.main_data.image} alt="person"/>
                               </div>
                           </div>
                       </div> : ""
                   }
               </div>
               <Modal isOpen={isOpen} toggle={() => {
                   setIsOpen(false);
               }} lg={true}>
                   <div className="register-modal-content">
                       <h3>{selectedCamera?.name}</h3>
                       {selectedCamera ?
                           <ReactPlayer className="video-wrap"  url={selectedCamera?.ddns_stream_url} playing={true} muted /> : ""
                       }
                   </div>
               </Modal>
           </div>

       </MainLayout>
    );
}

export default Register;
