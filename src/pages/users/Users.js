import React, {useEffect, useState} from 'react';
import MainLayout from "../../layout/MainLayout";
import Modal from "../../components/Modal";
import axios from "axios";
import {PATH_NAME} from "../../utils/constants";
import {toast} from "react-toastify";
import ReactPaginate from "react-paginate";

function Users(props) {
    const [isOpen, setIsOpen] = useState(false);
    const [users, setUsers] = useState([]);
    const [searchStr, setSearchStr] = useState('');
    const [userId, setUserId] = useState(null);

    const fetchData = async () => {
        try {
            const response = await axios.get(`${PATH_NAME}user/company/user/all?page=1&per_page=10`);
            setUsers(response.data);
        } catch (error) {
            toast.error('Error fetching data:', error);
        }
    };

    useEffect(() => {
        fetchData();
    }, []);

    const searchUser = async (e) => {
        setSearchStr(e.target.value);
        try {
            const response = await axios.get(`${PATH_NAME}user/company/user/all?search_str=${e.target.value}&page=1&per_page=10`);
            setUsers(response.data);
        } catch (error) {
            toast.error('Error fetching data:', error);
        }
    };

    const changePagination = async (page) => {
        return await axios.get(`${PATH_NAME}user/company/user/all?search_str=${searchStr}&page=${page}&per_page=10`)
            .then((res) => {
                return res.data
            });


    };
    const handleClick = async (event) => {
        let page = event.selected+1;
        const result = await changePagination(page);
        setUsers(result)
    };

    const handleDelete = async (user_id) => {
        setIsOpen(true);
        setUserId(user_id)
    };

    const deleteUser=async ()=>{
        const token=localStorage.getItem('access_token');
        setIsOpen(false);
        try {
            const response = await axios.delete(`${PATH_NAME}user/company/delete/user/${userId}`,{
                headers: {
                    Authorization: `Bearer ${token}`,
                },
            });

            if (response.status === 200 || response.status === 204) {
                toast.success("Deleted user");
                fetchData();
            } else {
                toast.error('Failed to delete:', response.statusText);
            }
        } catch (error) {
            toast.error('Error deleting data:', error.message);
        }
    };

    return (
        <MainLayout>
            <div className="users-page">
                <div className="users-page-header">
                    <div className="user-info">
                        <div className="user">
                            Cотрудники
                        </div>
                        <div className="user-job">
                            Управление сотрудниками
                        </div>
                    </div>
                    <div className="btn-group d-flex">
                        {/*<button className="import-btn">*/}
                        {/*    <img src="/assets/images/import.svg" alt=""/>*/}
                        {/*    Импорт из Excel*/}
                        {/*</button>*/}
                        <button className="add-btn" onClick={() => props.history.push("/users/add")}>
                            <img src="/assets/images/add.svg" alt=""/>
                            Добавить новое
                        </button>
                    </div>
                </div>
                <div className="line"/>
                <div className="users-page-body">
                    <div className="search-input">
                        <button>
                            <img src="/assets/images/search.svg" alt=""/>
                        </button>
                        <input
                            onChange={searchUser}
                            placeholder={"Поиск в таблице"}
                            type="search"/>
                    </div>
                    {/*<button className="filter-btn">*/}
                    {/*    <img src="/assets/images/filter.svg" alt=""/>*/}
                    {/*    Фильтр*/}
                    {/*</button>*/}
                </div>
                <div className="users-page-footer">
                    <table className="table">
                        <thead>
                        <tr>
                            <th>Имя и фамилия</th>
                            <th>Приоритет</th>
                            <th>Отдел</th>
                            <th>Должность</th>
                            <th>Электронная почта</th>
                            <th/>
                        </tr>
                        </thead>
                        <tbody>
                        {users?.items?.map((item, index) => (
                            <tr key={index}>
                                <td>
                                    <img style={{marginRight: "12px"}}
                                         src={item?.image} alt=""/>
                                    {item?.username}
                                </td>
                                <td>{item?.is_admin ? "Администратор":"Рабочий"}</td>
                                <td>{item?.department}</td>
                                <td>{item?.position}</td>
                                <td>{item?.email}</td>
                                <td>
                                    <img
                                        onClick={()=>props.history.push(`/users/edit/${item.id}`)}
                                        style={{width:"20px"}} src="/assets/images/edit.png" alt=""/>
                                    <img
                                        onClick={() => handleDelete(item.id)}
                                        src="/assets/images/delete.svg" alt=""/>
                                </td>
                            </tr>
                        ))}
                        </tbody>
                    </table>

                    <div className="d-flex justify-content-center mt-4">
                        <ReactPaginate
                            pageCount={Math.ceil(users?.count / 10)}
                            previousLabel={
                                <img
                                    style={{transform:"rotate(180deg)"}}
                                    src="/assets/images/arrow-right.svg" alt=""/>

                            }
                            nextLabel={
                                <img src="/assets/images/arrow-right.svg" alt=""/>

                            }
                            breakLabel={"..."}
                            marginPagesDisplayed={3}
                            onPageChange={handleClick}
                            containerClassName={"pagination"}
                            pageClassName={"page-item"}
                            pageLinkClassName={"page-link"}
                            previousClassName={"page-item prev-page-item"}
                            previousLinkClassName={"page-link"}
                            nextClassName={"page-item next-page-item"}
                            nextLinkClassName={"page-link"}
                            breakClassName={"page-item"}
                            breakLinkClassName={"page-link"}
                            activeClassName={"active"}

                        />
                    </div>
                </div>

                <Modal isOpen={isOpen} toggle={() => setIsOpen(false)}>
                    <div className="home-page-modal-content">
                        <h3>Внимание!</h3>
                        <div className="user-page-modal-content py-4">
                            Вы уверены, что удалите этого пользователя? Это <br/> невозможно отменить
                        </div>

                        <div className="user-page-modal-footer">
                            <button type="button" className="btn me-3"
                                    onClick={() => setIsOpen(false)}>Отменить
                            </button>
                            <button type="button" className="btn"
                                    onClick={deleteUser}>Да, удалить
                            </button>

                        </div>
                    </div>
                </Modal>
            </div>
        </MainLayout>
    );
}

export default Users;