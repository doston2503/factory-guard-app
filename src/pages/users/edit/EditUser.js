import React, { useEffect, useRef, useState } from 'react';
import MainLayout from '../../../layout/MainLayout';
import { Link } from 'react-router-dom';
import { toast } from 'react-toastify';
import axios from 'axios';
import { PATH_NAME } from '../../../utils/constants';

function EditUser(props) {
    const [image, setImage] = useState(null);
    const [imageFile, setImageFile] = useState(null);
    const fileInputRef = useRef(null);
    const [users, setUsers] = useState({});

    const fetchData = async () => {
        try {
            const response = await axios.get(`${PATH_NAME}user/company/user/${props.match.params.id}`);
            setUsers(response.data);
            setImage(response.data?.image);
        } catch (error) {
            toast.error('Error fetching data:', error);
        }
    };

    useEffect(() => {
        fetchData();
    }, []);

    const handleFileInputChange = (e) => {
        const file = e.target.files[0];
        setImageFile(file);
        if (file) {
            if (file.type.startsWith('image/')) {
                setImage(URL.createObjectURL(file));
            } else {
                toast.error('Please select a valid image file.');
            }
        } else {
            setImage(null);
        }
    };

    const resetImage = () => {
        console.log('worked');
        setImage(null);
        if (fileInputRef.current) {
            fileInputRef.current.value = '';
        }
    };

    const editUserForm = async (e) => {
        const token = localStorage.getItem('access_token');
        e.preventDefault();
        const formData = new FormData();

        formData.append('image', imageFile);
        formData.append('username', e.target.username?.value);
        formData.append('department', e.target.department?.value);
        formData.append('is_admin', e.target.is_admin?.checked);
        formData.append('position', e.target.position?.value);
        formData.append('email', e.target.email?.value);

        try {
            const response = await fetch(`${PATH_NAME}user/company/update/user/${props.match.params.id}`, {
                method: 'PUT',
                body: formData,
                headers: {
                    Authorization: `Bearer ${token}`,
                    Accept: 'application/json',
                },
            });

            if (response.ok) {
                toast.success('User update successful!');
                resetImage();
            } else {
                toast.error('Failed to update user:', response);
            }
        } catch (error) {
            toast.error('Error updating user:', error.message);
        }
    };

    return (
        <MainLayout>
            <div className="add-user-page">
                <div className="add-user-page-header">
                    <Link to={'/'}>
                        <img src="/assets/images/home.svg" alt="" />
                    </Link>
                    <img className="arrow-right-img" src="/assets/images/arrow-right.svg" alt="" />
                    <Link to={'/users'} className="users-link">
                        Cотрудники
                    </Link>
                    <img className="arrow-right-img" src="/assets/images/arrow-right.svg" alt="" />
                    <span>Изменить информацию о сотруднике</span>
                </div>
                <p className="title">Информация о новом сотруднике</p>
                <form onSubmit={editUserForm}>
                    <div className="upload-user-img">
                        {!image  && (
                            <label htmlFor="img">
                                <img src="/assets/images/upload-img.svg" alt="" />
                                <input
                                    id="img"
                                    name="img"
                                    type="file"
                                    accept="image/*"
                                    onChange={handleFileInputChange}
                                    ref={fileInputRef}
                                />
                            </label>
                        )}
                        {image && (
                            <div>
                                <img src={image} alt="Image Preview" className="show-user-image" />
                                <button type="button" onClick={resetImage}>
                                    Выбрать другое изображение
                                </button>
                            </div>
                        )}
                    </div>
                    <div className="upload-user-info">
                        <div className="d-flex">
                            <div>
                                <label htmlFor="username">Имя и фамилия</label>
                                <input required={true}
                                       defaultValue={users?.username}
                                       type="text" name="username" id="username"
                                       placeholder="Напишите полное имя"
                                       className="form-control"/>
                            </div>
                            <div>
                                <label htmlFor="department">Отдел</label>
                                <input
                                    defaultValue={users?.department}
                                    type="text" name="department" id="department"
                                    placeholder="Введите Отдел"
                                    className="form-control"/>
                            </div>
                        </div>

                        <div className="d-flex mt-3">
                            <div>
                                <label htmlFor="email">Электронная почта</label>
                                <input required={true}
                                       defaultValue={users?.email}
                                       type="email" name="email" id="email"
                                       placeholder="Напишите электронная почта"
                                       className="form-control"/>
                            </div>
                            <div>
                                <label htmlFor="position">Должность</label>
                                <input type="text" name="position"
                                       defaultValue={users?.position}
                                       placeholder="Введите Должность"
                                       id="position" className="form-control"/>
                            </div>
                        </div>

                        <div className="d-flex mt-4 align-items-center justify-content-between">
                            <div className="d-flex">
                                <div className="form-check">
                                    <input
                                        checked={users?.is_admin}
                                        className="form-check-input"
                                        type="checkbox" name="is_admin" id="is_admin"/>
                                    <label htmlFor="is_admin">Приоритет</label>
                                </div>
                            </div>

                            <div className="btn-groups">
                                <button type={"submit"}>Подтверждать</button>
                                <button type={"button"}
                                        onClick={() => props.history.push('/users')}
                                >
                                    <img src="/assets/images/arrow-left.svg" alt=""/>
                                    Назад
                                </button>
                            </div>
                        </div>

                    </div>
                </form>
            </div>
        </MainLayout>
    );
}

export default EditUser;


