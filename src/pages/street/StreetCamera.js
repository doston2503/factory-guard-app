import React, {useEffect, useState} from 'react';
import MainLayout from "../../layout/MainLayout";
import Select from 'react-select';
import axios from "axios";
import {CONFIG, PATH_NAME} from "../../utils/constants";
import ReactPlayer from "react-player";

function StreetCamera(props) {

    // const [cameras, setCameras] = useState([]);
    // const [selectedCamera, setSelectedCamera] = useState(null);
    // const [staticVideo,setStaticVideo]=useState([
    //     {id: 7, stream_url: "https://hls.cradle-vision.com/stream/smartcamera7/index.m3u8"},
    //     {id: 9, stream_url: "https://hls.cradle-vision.com/stream/smartcamera9/index.m3u8"},
    //     {id: 10, stream_url: "https://hls.cradle-vision.com/stream/smartcamera10/index.m3u8"},
    //     {id: 11, stream_url: "https://hls.cradle-vision.com/stream/smartcamera11/index.m3u8"},
    // ]);
    // const [showSelect, setShowSelect] = useState(false);
    // const [camerasForOption, setCamerasForOption] = useState([]);
    //
    //
    // useEffect(() => {
    //     getCameraList("");
    // }, []);
    //
    // const getCameraList = (search) => {
    //     axios.get(PATH_NAME + "company/camera/smartcamera/all?page=1&per_page=100&camera_type=entry&search_str=" + search, CONFIG)
    //         .then(response => {
    //             setCameras(response.data?.items);
    //             setCamerasForOption(response?.data?.items?.map(item => {
    //                 return {label: item.name, value: item.id}
    //             }));
    //             setTimeout(() => {
    //                 setShowSelect(true)
    //             }, 500)
    //         })
    // };
    //
    // const selectCamera = (e) => {
    //
    //     setSelectedCamera(staticVideo.filter(item => item.id == e.value)[0])
    // };

    return (
        <MainLayout>
            <div className="auto-counter-page">
                <div className="auto-counter-page-header">
                    <div className="d-flex justify-content-between">
                        <div>
                            <div className="title">Улица</div>
                            <div className="text">Проверка всех камеры</div>
                        </div>
                        {/*{showSelect ?*/}
                        {/*<Select*/}
                        {/*    defaultValue={camerasForOption[0]}*/}
                        {/*    className="search-select"*/}
                        {/*    classNamePrefix="select"*/}
                        {/*    isSearchable={true}*/}
                        {/*    placeholder="Камера"*/}
                        {/*    onInputChange={e => getCameraList(e)}*/}
                        {/*    name="camera"*/}
                        {/*    onChange={e => selectCamera(e)}*/}
                        {/*    options={cameras?.map(item => {return {label: item.name, value: item.id}})}*/}
                        {/*/>: ""*/}
                        {/*}*/}
                    </div>
                </div>
                <div className="line"/>
                <div className="street-page-body">
                    <ReactPlayer className="video-wrap"  url={"https://hls.cradle-vision.com/stream/factorycamera13/index.m3u8"} playing={true} muted />
                    {/*{selectedCamera ?*/}
                    {/*    <ReactPlayer className="video-wrap"  url={selectedCamera?.stream_url} playing={true} muted /> :*/}
                    {/*    cameras[0] ?*/}
                    {/*        <ReactPlayer className="video-wrap"  url={staticVideo.filter(item => item.id == cameras[0].id)[0]?.stream_url} playing={true} muted /> : ""*/}
                    {/*}*/}
                </div>
            </div>
        </MainLayout>
    );
}

export default StreetCamera;
