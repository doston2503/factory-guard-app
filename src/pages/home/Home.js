import React, {useEffect, useState} from 'react';
import MainLayout from "../../layout/MainLayout";
import Modal from "../../components/Modal";
import axios from "axios";
import {changeDate, changeDateSecond, CONFIG, PATH_NAME} from "../../utils/constants";
import ReactPlayer from "react-player";

function Home(props) {
    const [isOpen, setIsOpen] = useState(false);
    const [isOpenVideo, setIsOpenVideo] = useState(false);
    const [alerts, setAlerts] = useState([]);
    const [selectedAlert, setSelectedAlert] = useState(null);
    const [socket, setSocket] = useState(null);
    const [selectedCamera, setSelectedCamera] = useState(null);
    const [socketAlerts, setSocketAlerts] = useState([]);

    useEffect(() => {
        const data = getAlerts(5);

        const newSocket = new WebSocket('ws://178.62.213.208:9001/factory/all');

        newSocket.addEventListener('open', (event) => {
            console.log('WebSocket connection opened:', event);
        });

        newSocket.addEventListener('message', (event) => {
            const fixedJsonString = event.data.replace(/datetime\.datetime\(([^)]+)\)/g, '"$1"');
            const message = JSON.parse(fixedJsonString.replace(/'/g, '"'));
            if (message.row && message.row[0]) {
                axios.get(PATH_NAME + "company/alert/detail/" + message.row[0], CONFIG)
                    .then((res) => {
                        changeAlertData(res.data);
                    })
            }
            console.log('Message from server:', message);
        });

        newSocket.addEventListener('close', (event) => {
            console.log('WebSocket connection closed:', event);
        });

        newSocket.addEventListener('error', (event) => {
            console.error('WebSocket error:', event);
        });

        setSocket(newSocket);

        return () => {
            newSocket.close();
        };
    }, []);

    const getAlerts = async (minute) => {
        let response = await axios.get(PATH_NAME + "company/alert/all?interval_in_minutes=" + minute + "&page=1&per_page=100", CONFIG)
        setAlerts(response.data?.items);
        return response.data;
    }

    const openModal = (item) => {
        setSelectedAlert(item);
        setIsOpen(true)
    };

    const changeAlertData = (data) => {
        // setTimeout(() => {
            setAlerts((prevAlerts) => [data, ...prevAlerts]);
        // }, 1000 + (alerts.length * 1000))
    };

    return (
        <MainLayout>
            <div className="mainLayoutContentPadding">
                <div className="container-fluid p-0 home-page">
                    <div className="row">
                        <div className="col-xl-6">
                            <div className="main-card">
                                <div className="main-card-header">
                                    <div className="title">
                                        Состояние системы
                                    </div>
                                    <div className="text">
                                        Рабочие камеры
                                    </div>
                                </div>
                                <div className="main-card-body">
                                    <div className="box">
                                        <div className="count">
                                            2
                                        </div>
                                        <p>Умные камеры</p>
                                    </div>
                                    <div className="box">
                                        <img
                                            style={{transform: "translateY(-10px)"}}
                                            src="/assets/images/box1.svg" alt=""/>
                                    </div>
                                    <div className="box">
                                        <div className="count">
                                            6
                                        </div>
                                        <p>Безопасные камеры</p>
                                    </div>
                                </div>
                            </div>
                            <div className="main-card">
                                <div className="main-card-header">
                                    <div className="title">
                                        Состояние системы
                                    </div>
                                    <div className="text text-dangers">
                                        Не работающие камеры
                                    </div>
                                </div>
                                <div className="main-card-body">
                                    <div className="box">
                                        <div className="count">
                                            2
                                        </div>
                                        <p>Умные камеры</p>
                                    </div>
                                    <div className="box">
                                        <img style={{transform: "translateY(-20px)", width: "150px"}}
                                             src="/assets/images/box2.svg" alt=""/>
                                    </div>
                                    <div className="box">
                                        <div className="count">
                                            0
                                        </div>
                                        <p>Безопасные камеры</p>
                                    </div>
                                </div>
                            </div>
                            <div className="main-card">
                                <div className="main-card-header">
                                    <div className="title">
                                        Распознавание лица
                                    </div>
                                </div>
                                <div className="main-card-body">
                                    <div className="box">
                                        <div className="count">
                                            480
                                        </div>
                                        <p>Администратор</p>
                                    </div>
                                    <div className="box pt-3 mt-1">
                                        <div className="count">
                                            3452
                                        </div>
                                        <p>Рабочие</p>
                                    </div>
                                    <div className="box">
                                        <div className="count">
                                            4
                                        </div>
                                        <p>Не опознано</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="col-xl-6">
                            <div className="notification-card">
                                <div className="notification-card-header">
                                    <div className="title">
                                        Уведомления
                                    </div>
                                    <select name="" id="" onChange={e => getAlerts(e.target.value)}>
                                        <option value="5">Последние 5 минут</option>
                                        <option value="10">Последние 10 минут</option>
                                        <option value="30">Последние 30 минут</option>
                                        <option value="60">Последний 1 час</option>
                                        <option value="600">Последний 10 час</option>
                                    </select>
                                </div>

                                <div className="notification-card-body">
                                    {alerts?.map(item => (
                                        <div className="notification" key={item.id}>
                                            <div className="d-flex">
                                                {item?.capture_type === "intruder" ?
                                                    <div className="circle">
                                                        <img src="/assets/images/introdur.svg" alt=""/>
                                                    </div> :
                                                    item?.capture_type === "fall" ?
                                                        <div className="circle circle-fall">
                                                            <img src="/assets/images/fall.svg" alt=""/>
                                                        </div> :
                                                        item?.capture_type === "parking" ?
                                                            <div className="circle circle-parking">
                                                                <img src="/assets/images/parking.svg" alt=""/>
                                                            </div> :
                                                            item?.capture_type === "safety" ?
                                                                <div className="circle circle-safety">
                                                                    <img src="/assets/images/safety.svg" alt=""/>
                                                                </div> : ""
                                                }

                                                <div className="notification-info">
                                                    <div className="name">
                                                        {item.title}
                                                    </div>
                                                    <div className="text">
                                                        Время
                                                        <b className="ms-2">{changeDate(item?.created_at)}</b>
                                                    </div>
                                                </div>
                                            </div>
                                            <button type="button" className="detail-btn" onClick={() => openModal(item)}>
                                                Detail
                                            </button>
                                        </div>
                                    ))}
                                </div>
                            </div>
                        </div>
                    </div>
                    <Modal isOpen={isOpen} toggle={() => setIsOpen(false)}>
                        <div className="home-page-modal-content">
                            <h3>{selectedAlert?.title}</h3>
                            <div className="wrap d-flex justify-content-between">
                                <div className="wrap-item">
                                    <h4>Скриншоты:</h4>
                                    <img src={selectedAlert?.screenshot ? selectedAlert.screenshot : "/assets/images/alert.jpg"} alt="fall"/>
                                </div>
                                <div className="wrap-item">
                                    <h4>Location:</h4>
                                    <iframe
                                        src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d4342.620706580206!2d65.32869713708887!3d40.112032491914526!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3f51c700b94595d5%3A0x1bebc851c61c51a7!2z0J3QsNCy0L7QuNCw0LfQvtGC!5e1!3m2!1sru!2s!4v1701439772941!5m2!1sru!2s"
                                        style={{border: 0}} allowFullScreen="" loading="lazy" referrerPolicy="no-referrer-when-downgrade"></iframe>
                                </div>
                            </div>
                            <div className="info-wrap d-flex ">
                                <div className="info-wrap-item w-50 me-0">
                                    <span className="info-wrap-item-label">Time: </span>
                                    <span className="info-wrap-item-info">{changeDateSecond(selectedAlert?.created_at)}</span>
                                </div>
                                <div className="info-wrap-item  me-0 ms-2 w-50">
                                    <span className="info-wrap-item-label">Live video:</span>
                                    <a href="#" onClick={() => {
                                        setIsOpenVideo(true);
                                        setSelectedCamera(selectedAlert?.smartcamera)
                                    }} className="info-wrap-item-link">Video_001.mp4</a>
                                </div>

                            </div>
                            <div className="info-wrap d-flex ">
                                <div className="info-wrap-item me-0 w-50">
                                    <span className="info-wrap-item-label">Camera: </span>
                                    <span className="info-wrap-item-info">{selectedAlert?.smartcamera?.name}</span>
                                </div>

                                <div className="info-wrap-item me-0 ms-2 w-50">
                                    <span className="info-wrap-item-label">Description</span>
                                    <span className="info-wrap-item-info">{selectedAlert?.description}</span>
                                </div>
                            </div>
                            <button type="button" className="btn btn-primary" onClick={() => setIsOpen(false)}>Close</button>
                        </div>
                    </Modal>
                    <Modal isOpen={isOpenVideo} toggle={() => {
                        setIsOpenVideo(false);
                        setSelectedCamera(null)
                    }} lg={true}>
                        <div className="register-modal-content">
                            <h3>{selectedCamera?.name}</h3>
                            {console.log(selectedCamera)}
                            {selectedCamera ?
                                <ReactPlayer className="video-wrap" url={selectedCamera?.ddns_stream_url} playing={true} muted/> : ""
                            }
                        </div>
                    </Modal>
                </div>
            </div>
        </MainLayout>
    );
}

export default Home;
