import React, {useEffect, useState} from 'react';
import MainLayout from "../../layout/MainLayout";
import ReactPlayer from "react-player";
import Select from 'react-select';
import axios from "axios";
import {CONFIG, PATH_NAME} from "../../utils/constants";
import Modal from "../../components/Modal";

function AutoCounter(props) {
    const [cameras, setCameras] = useState([]);
    const [selectedCamera, setSelectedCamera] = useState(null);
    const [socket, setSocket] = useState(null);
    const [alerts, setAlerts] = useState([]);
    const [minute, setMinute] = useState(5);
    const [isOpen, setIsOpen] = useState(false);
    const [selectedAlert, setSelectedAlert] = useState(null);
    const [camerasForOption, setCamerasForOption] = useState([]);
    const [showSelect, setShowSelect] = useState(false);
    const [isFirst, setIsFirst] = useState(true);

    useEffect(() => {
        getCameraList("");

        return () => {
            if (socket) {
                socket.close();
            }
        };
    }, []);

    const getAlerts = async (minute, cameraId) => {
        let response = await axios.get(PATH_NAME + "company/alert/" + cameraId + "/alerts/all?interval_in_minutes=" + minute + "&page=1&per_page=100", CONFIG)
        setAlerts(response.data?.items);
        return response.data;
    };

    const connectToSocket = (cameraId) => {
        if (socket) {
            socket.close();
        }
        const newSocket = new WebSocket('ws://178.62.213.208:9001/factorysmart/' + cameraId);

        newSocket.addEventListener('open', (event) => {
            console.log('WebSocket connection opened:', event);
        });

        newSocket.addEventListener('message', (event) => {
            const fixedJsonString = event.data.replace(/datetime\.datetime\(([^)]+)\)/g, '"$1"');
            const message = JSON.parse(fixedJsonString.replace(/'/g, '"'));
            if (message.row && message.row[0]) {
                axios.get(PATH_NAME + "company/alert/detail/" + message.row[0], CONFIG)
                    .then((res) => {
                        changeAlertData(res.data);
                    })
            }
            console.log('Message from server:', message);
        });

        newSocket.addEventListener('close', (event) => {
            console.log('WebSocket connection closed:', event);
        });

        newSocket.addEventListener('error', (event) => {
            console.error('WebSocket error:', event);
        });

        setSocket(newSocket);
    };

    const changeAlertData = (data) => {
        // setTimeout(() => {
        setAlerts((prevAlerts) => [data, ...prevAlerts]);
        // }, 1000 + (alerts.length * 1000))
    };

    const getCameraList = (search) => {
        axios.get(PATH_NAME + "company/camera/smartcamera/all?page=1&per_page=100&search_str=" + search, CONFIG)
            .then(response => {
                setCameras(response.data?.items);
                setCamerasForOption(response?.data?.items?.map(item => {
                    return {label: item.name, value: item.id}
                }));
                if (isFirst && response.data?.items && response.data?.items[0]){
                    getAlerts(minute, response.data.items[0].id)
                    setIsFirst(false)
                }
                setTimeout(() => {
                    setShowSelect(true)
                }, 500)
            })
    };

    const selectCamera = (e) => {
        connectToSocket(e.value);
        getAlerts(minute, e.value)
        setSelectedCamera(cameras.filter(item => item.id == e.value)[0])

    };

    console.log(selectedCamera)
    return (
        <MainLayout>
            <div className="auto-counter-page">
                <div className="auto-counter-page-header">
                    <div className="d-flex justify-content-between">
                        <div>
                            <div className="title">Live аналитика</div>
                            <div className="text">Проверка всех камеры</div>
                        </div>
                        {/*{camerasForOption?.length > 0 ?*/}
                        {showSelect ?
                            <Select
                                defaultValue={camerasForOption[0]}
                                // defaultValue={camerasForOption[0]}
                                className="search-select"
                                classNamePrefix="select"
                                isSearchable={true}
                                placeholder="Камера"
                                onInputChange={e => getCameraList(e)}
                                name="camera"
                                onChange={e => selectCamera(e)}
                                options={camerasForOption}
                            />: ""
                        }
                        {/*:*/}
                        {/*   ""*/}
                        {/*}*/}
                    </div>
                </div>
                <div className="line"/>
                <div className="auto-counter-page-body">
                    <div className="image-section">

                        {selectedCamera ?
                            <ReactPlayer className="video-wrap"
                                         url={selectedCamera?.ddns_stream_url}
                                         playing={true}
                                         muted/> :
                            cameras[0] ?
                                <ReactPlayer className="video-wrap"
                                             url={cameras[0]?.ddns_stream_url}
                                             playing={true}
                                             muted/> : ""
                        }
                    </div>

                    <div className="notification-card">
                        <div className="notification-card-header">
                            <div className="title">
                                Уведомления
                            </div>
                            <select name="" id="" onChange={e => {
                                setMinute(e.target.value);
                                if (selectedCamera) {
                                    getAlerts(e.target.value, selectedCamera.id);
                                } else {
                                    if (alerts && alerts[0]) {
                                        getAlerts(e.target.value, alerts[0].id)
                                    }
                                }
                            }}>
                                <option value="5">Последние 5 минут</option>
                                <option value="10">Последние 10 минут</option>
                                <option value="30">Последние 30 минут</option>
                                <option value="60">Последний 1 час</option>
                                <option value="600">Последний 10 час</option>
                            </select>
                        </div>

                        <div className="notification-card-body">
                            <div className="table-section w-100">
                                <table className="table table-borderless">
                                    <thead>
                                    <tr>
                                        <th>Photo</th>
                                        <th>Info</th>
                                        <th style={{whiteSpace: 'nowrap'}}>Time and Date</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    {alerts?.map(item => (
                                        <tr key={item.id} onClick={() => {
                                            setSelectedAlert(item);
                                            setIsOpen(true)
                                        }}>
                                            <td>
                                                <div className="image-wrap">
                                                    <img src={item?.screenshot} alt=""/>
                                                    <div className="image-wrap-layer">
                                                        Подробнее
                                                    </div>
                                                </div>
                                            </td>
                                            <td>{item?.description}</td>
                                            <td style={{whiteSpace: "nowrap"}}>{item?.created_at?.substring(0, 10) + " " +
                                                item?.created_at?.substring(11, 16)
                                            }</td>
                                        </tr>
                                    ))}
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>


                </div>
                <Modal isOpen={isOpen} toggle={() => {
                    setIsOpen(false);
                    setSelectedAlert(null)
                }}>
                    <div className="pt-5">
                        <img src={selectedAlert?.screenshot} className="w-100" alt=""/>
                    </div>
                </Modal>
            </div>
        </MainLayout>
    );
}

export default AutoCounter;
