import React from 'react';
import {BrowserRouter, Route, Switch} from "react-router-dom";
import Home from "./pages/home/Home";
import FactoryArea from "./pages/factoryArea/FactoryArea";
import Register from "./pages/register/Register";
import Check from "./pages/check/Check";
import Users from "./pages/users/Users";
import AddUser from "./pages/users/add/AddUser";
import Login from "./pages/login/Login";
import {ToastContainer, toast} from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import Test from "./pages/test";
import AutoCounter from "./pages/autoCounter/AutoCounter";
import StreetCamera from "./pages/street/StreetCamera";
import EditUser from "./pages/users/edit/EditUser";
import Cameras from "./pages/Cameras";
import CameraDetail from "./pages/Cameras/CameraDetail";
function App(props) {
    return (
        <BrowserRouter>
            <Switch>
                <Route exact path="/" component={Home}/>
                <Route exact path="/login" component={Login}/>
                <Route exact path="/factory-area" component={FactoryArea}/>
                {/*<Route exact path="/register" component={Register}/>*/}
                {/*<Route exact path="/check" component={Check}/>*/}
                <Route exact path="/users" component={Users}/>
                <Route exact path="/users/add" component={AddUser}/>
                <Route exact path="/users/edit/:id" component={EditUser}/>
                <Route exact path="/live-analytics" component={AutoCounter}/>
                <Route exact path="/street-camera" component={StreetCamera}/>

                <Route exact path="/cameras" component={Cameras}/>
                <Route exact path="/cameras/:id" component={CameraDetail}/>
            </Switch>
            <ToastContainer/>
        </BrowserRouter>
    );
}

export default App;