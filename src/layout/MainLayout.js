import React, {useEffect, useState} from 'react';
import {Link, NavLink} from "react-router-dom";
import { useHistory } from 'react-router-dom';
function MainLayout({children}) {
    const history = useHistory();
    const [currentDate, setCurrentDate] = useState('');
    const [currentTime, setCurrentTime] = useState('');
    useEffect(() => {
        const now = new Date();
        const formattedDate = now.toLocaleString('ru-RU', {
            month: 'long',
            day: 'numeric',
        });

        const formattedTime = now.toLocaleString('en-US', {
            hour: 'numeric',
            minute: 'numeric',
        });
        setCurrentDate(formattedDate);
        setCurrentTime(formattedTime);

        const token = localStorage.getItem('access_token');
        if (!token) {
            localStorage.clear();
            history.push('/login');
        } else {
            // return children
        }
    }, []);


    return (
        <div className='mainLayout'>
            <div className="mainLayoutLeft">
                <div className="main-brand">
                    <Link to="/">
                        <img src="/assets/images/logo.svg" alt=""/>
                    </Link>
                    {/*<button>*/}
                    {/*    <img src="/assets/images/close-menu.svg" alt=""/>*/}
                    {/*</button>*/}
                </div>

                <ul className="main-menu">
                    <li>
                        <NavLink exact={true} to="/" activeClassName="active">
                            <img src="/assets/icon/home.svg" alt=""/>
                            Главная страница</NavLink>
                    </li>
                    <li>
                        <NavLink to="/factory-area" activeClassName="active">
                            <img src="/assets/icon/factory.svg" alt=""/>
                            Зона завода</NavLink>
                    </li>
                    {/*<li>*/}
                    {/*    <NavLink to="/register" activeClassName="active">*/}
                    {/*        <img src="/assets/icon/register.svg" alt=""/>*/}
                    {/*        Регистрироваться</NavLink>*/}
                    {/*</li>*/}
                    {/*<li>*/}
                    {/*    <NavLink to="/check" activeClassName="active">*/}
                    {/*        <img src="/assets/icon/check.svg" alt=""/>*/}
                    {/*        Проверить</NavLink>*/}
                    {/*</li>*/}
                    <li>
                        <NavLink to="/users" activeClassName="active">
                            <img src="/assets/icon/users.svg" alt=""/>
                            Cотрудники</NavLink>
                    </li>
                    <li>
                        <NavLink to="/live-analytics" activeClassName="active">
                            <img src="/assets/icon/live.svg" alt=""/>
                            Live аналитика</NavLink>
                    </li>
                    <li>
                        <NavLink to="/street-camera" activeClassName="active">
                            <img src="/assets/icon/street.svg" alt=""/>
                            Улица</NavLink>
                    </li>
                    <li>
                        <NavLink to="/cameras" activeClassName="active">
                            <img src="/assets/icon/cameras.svg" alt=""/>
                            Камеры</NavLink>
                    </li>
                </ul>

                <div className="user">
                    <div className="d-flex align-items-center">
                        <div className="user-img">
                            <img src="/assets/images/avatar.png" alt=""/>
                        </div>
                        <div className="user-info">
                            <div className="user-fullName">
                                Admin
                            </div>
                            <div className="user-phone">
                                94 222 19 98
                            </div>
                        </div>
                    </div>
                    {/*<div className="dots-img">*/}
                    {/*    <img src="/assets/images/dots.svg" alt=""/>*/}
                    {/*</div>*/}
                </div>
            </div>
            <div className="mainLayoutRight">
                <div className="mainLayoutHeader">
                    <div className="date">
                        <div className="d-flex align-items-center">
                            <div className="calendar-img me-2">
                                <img src="/assets/images/calendar.svg" alt=""/>
                            </div>
                            <div className="current-date">
                                {currentDate}
                            </div>
                        </div>
                        <div className="d-flex align-items-center ms-3">
                            <div className="time-img me-2">
                                <img src="/assets/images/time-clock.svg" alt=""/>
                            </div>
                            <div className="current-time">
                                {currentTime}
                            </div>
                        </div>
                    </div>

                    <div className="search-input">
                        <button>
                            <img src="/assets/images/search.svg" alt=""/>
                        </button>
                        <input
                            placeholder={"Искать в админке"}
                            type="search"/>
                    </div>
                </div>
                <div className="mainLayoutContent">
                    {children}
                </div>
            </div>
        </div>
    );
}

export default MainLayout;
