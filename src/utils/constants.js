export const PATH_NAME = 'http://178.62.213.208:8000/';

export const CONFIG = {
    headers: {
        "Authorization": "Bearer " + localStorage.getItem("access_token")
    }
}


// 2023-11-30T12:34:56
// 01.01.2023, 09:41
export const changeDate = (date) => {
    return date ? date.substring(8, 10) + "." + date.substring(5, 7) + "." + date.substring(0, 4) + ", " + date.substring(11, 13) + ":" + date.substring(14, 16) : ""
}

export const changeDateSecond = (date) => {
    return date ? date.substring(11, 13) + ":" + date.substring(14, 16) + " " + date.substring(8, 10) + "." + date.substring(5, 7) + "." + date.substring(0, 4) : ""
}

export const getCurrentDateTime = () => {
    const currentDate = new Date();
    const formattedDate = currentDate.toLocaleDateString('ru-RU', {
        day: '2-digit',
        month: '2-digit',
        year: 'numeric',
    });
    const formattedTime = currentDate.toLocaleTimeString('en-GB', {
        hour: '2-digit',
        minute: '2-digit',
    });
    return `${formattedDate} ${formattedTime}`;
};